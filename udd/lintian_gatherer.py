#!/usr/bin/env python

"""
This script imports lintian run results into the database
See lintian.debian.org
"""

from __future__ import print_function

from gatherer import gatherer
import json
import os.path
import re
import lzma
import sys


def get_gatherer(connection, config, source):
    return LintianGatherer(connection, config, source)


class LintianGatherer(gatherer):
    extension_to_package_type_map = {
        "dsc": "source",
        "deb": "binary",
        "udeb": "udeb",
        "changes": "changes",
        "buildinfo": "buildinfo",
    }

    excluded_tags = [
        "octal-permissions",
        "trimmed-deb822-field",
    ]

    def __init__(self, connection, config, source):
        gatherer.__init__(self, connection, config, source)
        self.assert_my_config('path', 'table')

    def run(self):
        my_config = self.my_config

        # making space for new data
        cur = self.cursor()

        cur.execute("DELETE FROM %s" % my_config["table"])

        cur.execute("""PREPARE lintian_insert
          AS INSERT INTO %s (package, package_type, package_version, package_arch, tag, tag_type, information)
          VALUES ($1, $2, $3, $4, $5, $6, $7)""" % (my_config['table']))

        entries = []

        basepath = os.path.join(my_config['path'], 'results')

        with open(os.path.join(basepath, 'list.xz')) as f:
            # PY3: the file needs to be read in 'rb' and then turned into str
            lines = lzma.decompress(f.read()).splitlines()

        file_name_regex = re.compile(r'^([^_]+)_([^_]+)(?:_([^_]+))?\.([^.]+)$')

        for line in lines:
            # we are totally assuming the files are xz-compressed
            xzjsonpath = line.strip()

            with open(os.path.join(basepath, xzjsonpath)) as f:
                # PY3: the open() should be 'rb'
                raw_data = lzma.decompress(f.read())
                if not raw_data:
                    continue
                try:
                    results = json.loads(raw_data)
                except ValueError:
                    # print()
                    # sys.stdout.flush()
                    print("Failed to parse", xzjsonpath, file=sys.stderr)
                    # sys.stderr.flush()
                    # print()
                    continue

                for group in results['groups']:
                    for input_file in group['input-files']:

                        basename = os.path.basename(input_file['path'])

                        match = file_name_regex.match(basename)
                        if not match:
                            print('Cannot parse file name %s' % basename, file=sys.stderr)
                            continue

                        (package_name, version, architecture, extension) = match.groups()

                        package_type = LintianGatherer.extension_to_package_type_map[extension]
                        if architecture is None:
                            architecture = 'source'

                        for tag in input_file['tags']:
                            visibility = tag['severity']

                            # adjust to UDD wording
                            if visibility == 'info':
                                visibility = 'information'

                            if "experimental" in tag:
                                visibility = 'experimental'

                            if "override" in tag:
                                visibility = 'overridden'

                            if tag['name'] in LintianGatherer.excluded_tags:
                                continue

                            entries.append((
                                package_name,
                                package_type,
                                version,
                                architecture,
                                tag['name'],
                                visibility,
                                tag.get('context', ""),
                            ))
            # print('.', end='')
            # sys.stdout.flush()

        # print()
        cur.executemany("EXECUTE lintian_insert (%s, %s, %s, %s, %s, %s, %s)", entries)
        cur.execute("DEALLOCATE lintian_insert")
        cur.execute("ANALYZE %s" % my_config["table"])


if __name__ == '__main__':
    LintianGatherer().run()

# vim:set et tabstop=4:
