#!/usr/bin/ruby -w
# encoding: utf-8
#
# FIXME add detection of unknown fields

$:.unshift  '/srv/udd.debian.org/udd/udd'

require 'yaml'
require 'pg'
require 'json'
require 'open-uri'

DODESC=false

DEBUG = false
TESTMODE = false
URL = 'https://mentors.debian.net/api/uploads/'

# PG doc: http://deveiate.org/code/pg/

class MentorsGatherer
  def initialize
    config = YAML::load(IO::read(ARGV[0]))
    gconf = config['general']

    @db = PG.connect({ :dbname => gconf['dbname'], :port => (gconf['dbport'] || 5432)})
#    @db.trace(STDOUT)
#    @db.setnonblocking(true)

    @db.prepare 'uploads_insert', <<-EOF
    INSERT INTO mentors_raw_uploads (
        id,
        changes,
        closes,
        component,
        distribution,
        package,
        uploaded,
        uploader,
        version
    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
    EOF
  end

  def run
    @db.exec("BEGIN")
    @db.exec("SET CONSTRAINTS ALL DEFERRED")

    @db.exec("DELETE FROM mentors_raw_uploads")
    res = JSON::parse(open(URL, 'rb').read)
    res.each { |r| @db.exec_prepared('uploads_insert',
                                     [r['id'],
                                     r['changes'],
                                     r['closes'].join(" "),
                                     r['component'],
                                     r['distribution'],
                                     r['package'],
                                     r['uploaded'],
                                     r['uploader'],
                                     r['version']]
                                    ) }

    @db.exec("COMMIT")
    @db.exec("ANALYSE mentors_raw_uploads")
  end
end


if(ARGV.length != 3)
  puts "Usage: #{$0} <config>"
  exit(1)
end
MentorsGatherer::new.run
